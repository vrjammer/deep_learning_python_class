import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

import numpy as np
import matplotlib.pyplot as plt

Nclass = 500

# create a gaussian cloud centered on 0, -2
X1 = np.random.randn(Nclass, 2) + np.array([0, -2])
# create a second gaussian cloud centered on 2, 2
X2 = np.random.randn(Nclass, 2) + np.array([2, 2])
# third cloud centered at -2,2
X3 = np.random.randn(Nclass, 2) + np.array([-2, 2])
# stack those three arrays vertically
X = np.vstack([X1, X2, X3])

# create labels 
Y = np.array([0]*Nclass + [1]*Nclass + [2]*Nclass)

plt.scatter(X[:,0], X[:,1], c=Y, s=100, alpha=0.5)
plt.show()

#number of inputs = 2
D = 2
#number of hidden neurons = 3
M = 3
#number of classes (output neurons) = 3
K = 3

# initialize the weights to random values
W1 = np.random.randn(D, M)
b1 = np.random.randn(M)
W2 = np.random.randn(M, K)
b2 = np.random.randn(K)

def forward(X, W1, b1, W2, b2):
	# result of first layer using sigmoid function
	Z = 1 / (1 + np.exp(-X.dot(W1) - b1))
	# then result of second layer uses softmax function
	A = Z.dot(W2) + b2
	expA = np.exp(A)
	Y = expA / expA.sum(axis=1, keepdims=True)
	return Y

def classification_rate(Y, P):
	n_correct = 0
	n_total = 0
	for i in range(len(Y)):
		n_total += 1
		if Y[i] == P[i]:
			n_correct += 1
	return float(n_correct) / n_total

P_Y_given_X = forward(X, W1, b1, W2, b2)

P = np.argmax(P_Y_given_X, axis=1)

assert(len(P) == len(Y))

print ("Classification rate for randomly chosen weights:", classification_rate(Y, P))
