# Is it really a probability though ?
## Gregory · Lecture 10 · 2 days ago
Is it really a probability though ?

Where, say, given X if you predict y = k you will be right p of the time and wrong (1-p) of the time ?

It seems that we want to call it a probability for various reasons but not because it's related to a statistical interpretation ? Or is there another way to interpret this as a probability that I'm missing ?

## Lazy Programmer — Instructor
· 2 days ago

Here's a simple exercise

model1 = logistic regression

model2 = neural network

Now they both output different probabilities given some input. Which one is the "true" probability?

## Gregory
· 2 days ago

I'd say neither ?

## Lazy Programmer — Instructor
· 2 days ago

The answer is that a model is not reality.

We are finding an approximation to whatever system generated the data.

Therefore, to ask "is the output a probability" is just semantics - it's a probability because the model says it is - it is part of the model.

A related question: "is the probability 'right'?" The answer is that if the model assumptions are true, and each of the steps you took to arrive at the answer are logically consistent, then the solution is (logically) "right".


Using the frequentist perspective of probability, if a model outputs p(y=1 | x) = 0.9 (and y can only be 1 or 0), does that mean the same data will generate a 1 90% of the time and a 0 10% of the time?

Of course no since models cannot even agree on what the probability should be.

Typically with a real-valued input x, all your data points are unique. So for example I only have 1 sample where x = 5 --> y = 1. Therefore, the "maximum likelihood" probability p(y=1 | x=5) must be 1.

However clearly that is not a good solution since if x=6 and it does not appear in the dataset then we cannot make a prediction at all.

But since we know there is a geometric aspect we can apply a model (which may or may not make assumptions that are true).

## Gregory
· a day ago

Here's another way to look at it (and where I come from). 

Let's say you have a very simple model where voters of party A and voters of party B live in a town separated by zip codes. Given the zip code the model will tell you : there's 90% chance that this person is a voter of party A. And if we sampled every person living in that zip code then we'll find out that yes 9 out of 10 voters in that zip code voted for party A. In this case the model matches reality and it is a good prediction of an actual probability. (we do that very often with polling I guess ? we "train" on a few samples and find the parameters that predict the result of an election).

But with the way we train our models, any answer between 0.5 and 1 would be considered a success, right ? So yes it's probably just semantics and I probably miss the big picture ;) but I was wondering if there was anything more to why we call them probabilities here (I guess I'll just follow along with the course and this may become clearer).

## Lazy Programmer — Instructor
· a day ago Answer

> But with the way we train our models, any answer between 0.5 and 1 would be considered a success, right ?

Given your example, technically no - since the cost would be:

9 * 1 log p + 1 * log ( 1 - p ) = 0 --> optimize wrt p --> p = 0.9 precisely


It's when you add other assumptions to the model that p changes, e.g. p = f(x) = w*x

Now you are not optimizing wrt p, you are optimizing wrt w.

So it becomes a question of "is the model right?"

It's no longer "is the probability right?" because the probability must be right if the model assumptions are true (unless there are mistakes in the equations, which there are not).

## Okay I think I get it. 

Is there a particular reasoning that led to choose this approach for classification versus another version that would use an error function that would look like 

Sum (tn-yn)^2

(just one example among the many functions we could think of in this scenario).

This is a model that wouldn't answer the poll question correctly, but I think would correctly identify (given it's sufficiently complex) that voter of that zipcode is likely voter of party A (if that's all we require of it). Accuracy in poll predictions may be one thing, but another thing might be that we do not misidentify too many voters 
