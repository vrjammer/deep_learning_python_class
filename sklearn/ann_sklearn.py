# regression.py do a linear regression of the simple y=x1x2 problem
# code mostly copied from logistic_softmax_train.py with some changes
# to adapt to the new problem
import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

from sklearn.neural_network import MLPClassifier
import numpy as np 
import matplotlib.pyplot as plt 

#dimensionality of the problem
D = 2
M = 5
K = 3

# create random training data again
Nclass = 500

# three gaussian clouds
X1 = np.random.randn(Nclass, D) + np.array([0, -2])
X2 = np.random.randn(Nclass, D) + np.array([2, 2])
X3 = np.random.randn(Nclass, D) + np.array([-2, 2])
X = np.vstack([X1, X2, X3]).astype(np.float32)

Y = np.array([0]*Nclass + [1]*Nclass + [2]*Nclass)

# let's see what it looks like
plt.scatter(X[:,0], X[:,1], c=Y, s=100, alpha = 0.5)
plt.show()

N = len(Y)
#turn Y into an indicator matrix for training
T = np.zeros((N, K))
for i in range(N):
	T[i, Y[i]] = 1

# MLPClassifier stands for multi layer perceptron classifier
# there's similarly a Multi layer perceptron regressor (MLPRegressor)
# refer to SK learn MLPClassifier doc for a complete list of arguments and knobs !
classifier = MLPClassifier(hidden_layer_sizes=(M), activation='relu', verbose=True)

classifier.fit(X, T)

py_X = classifier.predict(X)
pred = np.argmax(py_X, axis=1)
print("accuracy after training: ", np.mean(Y == pred))


