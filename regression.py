# regression.py do a linear regression of the simple y=x1x2 problem
# code mostly copied from logistic_softmax_train.py with some changes
# to adapt to the new problem
import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

import numpy as np 
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#number of inputs = 2
D = 2
#number of hidden neurons = 3
M = 100

N = 500
# create a uniform cloud between (-2, +2)
X = np.random.random((N, D)) * 4 - 2
Y = X[:,0]*X[:,1] # makes a saddle shape

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X[:,0], X[:,1], Y)
plt.show()

Xtrain = X[:-100]
Ytrain = Y[:-100]

Xtest = X[-100:]
Ytest = Y[-100:]

# weights are initialized to random gaussian weights
W1 = np.random.randn(D, M) / np.sqrt(D)
b1 = np.zeros(M)
W2 = np.random.randn(M) / np.sqrt(M)
b2 = 0

def forward(X, W1, b1, W2, b2):
	# result of first layer using relu function
	#Z = np.maximum(X.dot(W1) + b1, 0)
	Z = X.dot(W1) + b1
	Z = Z * (Z > 0)
	# then result of second layer uses linear combination
	return Z, Z.dot(W2) + b2

def derivative_w2(Z, TminusY):
	return TminusY.dot(Z)

def derivative_b2(TminuY):
	return TminuY.sum(axis=0)

def derivative_w1_b1(X, Z, TminuY, W2):
	dZ = np.outer(TminuY, W2) * (Z > 0)
	return X.T.dot(dZ), dZ.sum(axis=0)

def cost(TminusY):
	# mean square error cost
	return np.mean(TminusY ** 2)



train_costs = []
test_costs = []
learning_rate = 0.0001
for i in range(100000):
	Ztrain, pYtrain  = forward(Xtrain, W1, b1, W2, b2)
	Ztest, pYtest    = forward(Xtest,  W1, b1, W2, b2)

	TminuYtrain = Ytrain - pYtrain
	TminuYtest = Ytest - pYtest

	ctrain = cost(TminuYtrain)
	ctest  = cost(TminuYtest)
	train_costs.append(ctrain)
	test_costs.append(ctest)

	W2 += learning_rate * derivative_w2(Ztrain, TminuYtrain)
	b2 += learning_rate * derivative_b2(TminuYtrain)
	dW1, db1 = derivative_w1_b1(Xtrain, Ztrain, TminuYtrain, W2)		
	W1 += learning_rate * dW1
	b1 += learning_rate * db1
	if i % 500 == 0:
		print("test error: ", ctest)

print ("Final train error: ", ctrain)
print ("Final test error: ",  ctest)

legend1, = plt.plot(train_costs, label='train cost')
legend2, = plt.plot(test_costs, label='test cost')
plt.legend([legend1, legend2])
plt.show()

# plot the prediction with the data

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X[:,0], X[:, 1], Y)

# surface plt
line = np.linspace(-2, 2, 20)
xx, yy = np.meshgrid(line, line)
Xgrid = np.vstack((xx.flatten(), yy.flatten())).T
_, Yhat = forward(Xgrid, W1, b1, W2, b2)
ax.plot_trisurf(Xgrid[:,0], Xgrid[:,1], Yhat, linewidth=0.2, antialiased=True)
plt.show()

#plot the magnitude of the residuals
Ygrid = Xgrid[:,0]*Xgrid[:,1]
R= np.abs(Ygrid - Yhat)

plt.scatter(Xgrid[:,0], Xgrid[:,1], c=R)
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_trisurf(Xgrid[:,0], Xgrid[:, 1], R, linewidth=0.2, antialiased=True)
plt.show()

