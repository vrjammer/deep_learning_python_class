import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

import numpy as np 
# we'll use the get_data function that we defined in the file called process.py
from process import get_data

X, Y = get_data()

# arbitrarily choose 5 hidden units
M = 5
D = X.shape[1]
K = len(set(Y))
# For now, weights for layer 1 are random gaussian distribution, no bias
W1 = np.random.randn(D, M)
b1 = np.zeros(M)
# For now, weights for layer 2 are random gaussian distribution, no bias
W2 = np.random.randn(M, K)
b2 = np.zeros(K)

def softmax(a):
	expA = np.exp(a)
	return expA / expA.sum(axis=1, keepdims=True)

def forward(X, W1, b1, W2, b2):
	Z = np.tanh(X.dot(W1) + b1)
	return softmax(Z.dot(W2) + b2)

P_Y_given_X = forward(X, W1, b1, W2, b2)

predictions = np.argmax(P_Y_given_X, axis=1)

def classification_rate(Y, P):
	return np.mean(Y == P)

print("Score: ", classification_rate(Y, predictions))
