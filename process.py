import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

import numpy as np
import pandas as pd

def get_data():
	# read the data from the csv file
	df = pd.read_csv('ecommerce_data.csv')
	# turn the data into a numpy matrix since it's easier to work with in numpy
	data = df.as_matrix()
	# split the data, X is everything x everything except the last column
	X = data[:, :-1]
	# Y is everything x the last column
	Y = data[:, -1]
	# normalize the first column / num products viewed (mean = 0, standard deviation = 1)
	X[:,1] = (X[:,1] - X[:,1].mean()) / X[:,1].std()
	# then the second column / time spent on site
	X[:,2] = (X[:,2] - X[:,2].mean()) / X[:,2].std()
	N, D = X.shape
	# Set everything to zero, 
	# reserve three extra columns for the one-hot encoding of the time of day
	X2 = np.zeros((N, D+3))
	X2[:,0:(D-1)] = X[:, 0:(D-1)]

	# Now do the one hot encoding for the last column (time of day)
	for n in range(N):
		t = int(X[n, D-1])
		X2[n, t+D-1] = 1
	#another way to do one hot encoding
	Z = np.zeros((N, 4))
	Z[np.arange(N), X[:, D-1].astype(np.int32)] = 1
	# X2[:, -4:] = Z
	assert(np.abs(X2[:,-4:] - Z).sum() < 10e-10)

	return X2, Y

def get_binary_data():
	# the data contains more than 1 class, so restrict the data set to only one class
	# for this exercise (Y could be 0,1,2 restrict it to 0,1)
	X, Y = get_data()
	X2 = X[Y <= 1]
	Y2 = Y[Y <= 1]
	return X2, Y2

X2, Y2 = get_binary_data()

X2
Y2
