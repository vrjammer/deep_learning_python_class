#using a single layer logistic regression for multi class detection
import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

import numpy as np 
import matplotlib.pyplot as plt 

from util import getData, softmax, cost, y2indicator, error_rate
from sklearn.utils import shuffle

class LogisticModel(object):
	def __init__(self):
		pass

	def fit(self, X, Y, learning_rate=10e-7, reg=10e-12, epochs=10000, show_fig=False):
		Xvalid, Yvalid = X[-10000:], Y[-10000:]
		Tvalid = y2indicator(Yvalid)
		X, Y = X[:-10000], Y[:-10000]
		X, Y = shuffle(X, Y)

		N, D = X.shape
		K = len(set(Y))
		T = y2indicator(Y)
		self.W = np.random.randn(D, K) / np.sqrt(D + K)
		self.b = np.zeros(K)

		costs = []
		best_validation_error = 1

		for i in range(epochs):
			pY = self.forward(X)

			#gradient descent
			self.W -= learning_rate * (X.T.dot(pY-T) + reg * self.W)
			self.b -= learning_rate * ((pY-T).sum(axis=0) + reg*self.b)

			if i % 10 == 0:
				pYvalid = self.forward(Xvalid)
				c = cost(Tvalid, pYvalid)
				costs.append(c)
				e = error_rate(Yvalid, np.argmax(pYvalid, axis=1))
				print("i: ", i, "cost: ", c, "error: ", e)
				if e < best_validation_error:
					best_validation_error = e
		print("best_validation_error: ", best_validation_error)

		if show_fig:
			plt.plot(costs)
			plt.show()

	def forward(self, X):
		return softmax(X.dot(self.W) + self.b)

	def predict(self, X):
		pY = self.forward(X)
		return np.argmax(pY, axis=1)

	def score(self, X, Y):
		prediction = self.predict(X)
		return 1 - error_rate(Y, prediction)

def main():
	X, Y = getData()

	model = LogisticModel()
	model.fit(X, Y, show_fig=True)
	print("score: ", model.score(X, Y))

if __name__ == '__main__':
	main()

