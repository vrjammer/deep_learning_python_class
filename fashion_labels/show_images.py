# https://www.kaggle.com/zalando-research/fashionmnist
import numpy as np 
import matplotlib.pyplot as plt 

from util import getData

label_map = ['Top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

def main():
	X, Y = getData(balance_ones=False)

	while True:
		for i in range(10):
			x, y = X[Y==i], Y[Y==i]
			N = len(y)
			j = np.random.choice(N)
			plt.imshow(x[j].reshape(28, 28), cmap='gray')
			plt.title(label_map[y[j]])
			plt.show()
		prompt = raw_input('Quit? Enter Y:\n')
		if prompt =='Y':
			break

if __name__ == '__main__':
	main()
