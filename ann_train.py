import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

import numpy as np 
import matplotlib.pyplot as plt

from sklearn.utils import shuffle
# reuse the get_data function we've written in process.py
from process import get_data

# convert from a label (0..K-1) to an indicator matrix [[0, 0,.. 1,..0, 0]]
def y2indicator(y, K):
	N = len(y)
	ind = np.zeros((N, K))
	for n in range(N):
		ind[n, y[n]] = 1
	return ind

def softmax(a):
	expA = np.exp(a)
	return expA / expA.sum(axis=1, keepdims=True)
	
# copy pasted from forwardprop.py
def forward(X, W1, b1, W2, b2):
	# result of first layer using sigmoid function
	Z = 1 / (1 + np.exp(-X.dot(W1) - b1))
	# then result of second layer uses softmax function
	A = Z.dot(W2) + b2
	expA = np.exp(A)
	Y = expA / expA.sum(axis=1, keepdims=True)
	return Y, Z

def derivative_w2(Z, T, Y):
	return Z.T.dot(T - Y)

def derivative_b2(T, Y):
	return (T - Y).sum(axis=0)

def derivative_w1(X, Z, T, Y, W2):
	dZ = (T-Y).dot(W2.T) * Z * (1-Z)
	return X.T.dot(dZ)

def derivative_b1(T, Y, W2, Z):
	return ((T-Y).dot(W2.T) * Z * (1-Z)).sum(axis=0)

def predict(P_Y_given_X):
	return np.argmax(P_Y_given_X, axis=1)

def classification_rate(Y, P):
	return np.mean(Y == P)

def cross_entropy(T, pY):
	return -np.mean(T*np.log(pY))

def main():
	X, Y = get_data()
	X, Y = shuffle(X, Y)
	Y = Y.astype(np.int32) # make sure Y are integers and not floating point values
	D = X.shape[1]
	M = 5

	# determines how many different values are encountered in the vector Y
	# -> builds a set of all values, then count how many individual values are in the set
	K = len(set(Y))

	Xtrain = X[:-100]
	Ytrain = Y[:-100]
	Ytrain_ind = y2indicator(Ytrain, K)

	Xtest = X[-100:]
	Ytest = Y[-100:]
	Ytest_ind = y2indicator(Ytest, K)

	# weights are initialized to random gaussian weights
	W1 = np.random.randn(D, M)
	b1 = np.zeros(M)
	W2 = np.random.randn(M, K)
	b2 = np.zeros(K)

	train_costs = []
	test_costs = []
	learning_rate = 1e-4
	for i in range(100000):
		pYtrain, Ztrain = forward(Xtrain, W1, b1, W2, b2)
		pYtest, Ztest   = forward(Xtest,  W1, b1, W2, b2)

		ctrain = cross_entropy(Ytrain_ind, pYtrain)
		ctest  = cross_entropy(Ytest_ind,  pYtest)
		train_costs.append(ctrain)
		test_costs.append(ctest)

		W2 += learning_rate*derivative_w2(Ztrain, Ytrain_ind, pYtrain)
		b2 += learning_rate*derivative_b2(Ytrain_ind, pYtrain)
		W1 += learning_rate*derivative_w1(Xtrain, Ztrain, Ytrain_ind, pYtrain, W2)		
		b1 += learning_rate*derivative_b1(Ytrain_ind, pYtrain, W2, Ztrain)

		if i % 5000 == 0:
			predicted = predict(pYtest)
			rate = classification_rate(Ytest, predicted)
			print("test error: ", ctest, "Classification rate: ", rate)

	print ("Final train classification_rate: ", classification_rate(Ytrain, predict(pYtrain)))
	print ("Final test classification_rate: ", classification_rate(Ytest, predict(pYtest)))

	legend1, = plt.plot(train_costs, label='train cost')
	legend2, = plt.plot(test_costs, label='test cost')
	plt.legend([legend1, legend2])
	plt.show()

if __name__ == '__main__':
	main()
