# regression.py do a linear regression of the simple y=x1x2 problem
# code mostly copied from logistic_softmax_train.py with some changes
# to adapt to the new problem
import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

import tensorflow as tf
import numpy as np 
import matplotlib.pyplot as plt 

# create random training data again
Nclass = 500
D = 2
M = 10
K = 3

# three gaussian clouds
X1 = np.random.randn(Nclass, D) + np.array([0, -2])
X2 = np.random.randn(Nclass, D) + np.array([2, 2])
X3 = np.random.randn(Nclass, D) + np.array([-2, 2])
X = np.vstack([X1, X2, X3]).astype(np.float32)

Y = np.array([0]*Nclass + [1]*Nclass + [2]*Nclass)

# let's see what it looks like
plt.scatter(X[:,0], X[:,1], c=Y, s=100, alpha = 0.5)
plt.show()

N = len(Y)
#turn Y into an indicator matrix for training
T = np.zeros((N, K))
for i in range(N):
	T[i, Y[i]] = 1

def init_weights(shape):
	return tf.Variable(tf.random_normal(shape, stddev=0.01))

def forward(X, W1, b1, W2, b2):
	Z = tf.nn.sigmoid(tf.matmul(X, W1) + b1)
	return tf.matmul(Z, W2) + b2

tfX = tf.placeholder(tf.float32, [None, D])
tfY = tf.placeholder(tf.float32, [None, K])

W1 = init_weights([D, M])
b1 = init_weights([M])
W2 = init_weights([M, K])
b2 = init_weights([K])

py_X = forward(tfX, W1, b1, W2, b2)

sess = tf.Session()
init = tf.initialize_all_variables()
sess.run(init)


with tf.device('/cpu:0'):
	cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=py_X, labels=tfY))
	train_op = tf.train.GradientDescentOptimizer(0.1).minimize(cost)
	predict_op = tf.argmax(py_X, 1)

for i in range(10000):
	with tf.device('/cpu:0'):
		sess.run(train_op, feed_dict = {tfX: X, tfY: T})
	if i % 100 == 0:
		pred = sess.run(predict_op, feed_dict={tfX: X, tfY: T})
		print("accuracy:", np.mean(Y == pred))





