import sys

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

import numpy as np 
import matplotlib.pyplot as plt

from sklearn.utils import shuffle
# reuse the get_data function we've written in process.py
from process import get_data

# convert from a label (0..K-1) to an indicator matrix [[0, 0,.. 1,..0, 0]]
def y2indicator(y, K):
	N = len(y)
	ind = np.zeros((N, K))
	for n in range(N):
		ind[n, y[n]] = 1
	return ind

X, Y = get_data()
X, Y = shuffle(X, Y)
Y = Y.astype(np.int32) # make sure Y are integers and not floating point values
D = X.shape[1]

# determines how many different values are encountered in the vector Y
# -> builds a set of all values, then count how many individual values are in the set
K = len(set(Y))

Xtrain = X[:-100]
Ytrain = Y[:-100]
Ytrain_ind = y2indicator(Ytrain, K)

Xtest = X[-100:]
Ytest = Y[-100:]
Ytest_ind = y2indicator(Ytest, K)

# weights are initialized to random gaussian weights
W = np.random.randn(D, K)
b = np.zeros(K)

def softmax(a):
	expA = np.exp(a)
	return expA / expA.sum(axis=1, keepdims=True)
	
def forward(X, W, B):
	return softmax(X.dot(W) + b)

def predict(P_Y_given_X):
	return np.argmax(P_Y_given_X, axis=1)

def classification_rate(Y, P):
	return np.mean(Y == P)

def cross_entropy(T, pY):
	return -np.mean(T*np.log(pY))

train_costs = []
test_costs = []
learning_rate = 0.001
for i in range(10000):
	pYtrain = forward(Xtrain, W, b)
	pYtest  = forward(Xtest,  W, b)

	ctrain = cross_entropy(Ytrain_ind, pYtrain)
	ctest  = cross_entropy(Ytest_ind,  pYtest)
	train_costs.append(ctrain)
	test_costs.append(ctest)

	W -= learning_rate*Xtrain.T.dot(pYtrain - Ytrain_ind)
	b -= learning_rate*(pYtrain - Ytrain_ind).sum(axis=0)
	if i % 1000 == 0:
		predicted = predict(pYtest)
		rate = classification_rate(Ytest, predicted)
		print("test error: ", ctest, "Classification rate: ", rate)

print ("Final train classification_rate: ", classification_rate(Ytrain, predict(pYtrain)))
print ("Final test classification_rate: ", classification_rate(Ytest, predict(pYtest)))

legend1, = plt.plot(train_costs, label='train cost')
legend2, = plt.plot(test_costs, label='test cost')
plt.legend([legend1, legend2])
plt.show()




